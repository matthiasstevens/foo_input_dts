// DTS Coherent Acoustics decoder for foobar2000
// Originally based on dtsdec, Copyright (C) 2004 Gildas Bazin <gbazin@videolan.org>
//                                       (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
//                                       (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
// Now based on dcadec, Copyright (C) 2015 foo86
// foobar2000 component copyright (C) 2004-2006 Janne Hyv�rinen
//                                (C) 2006-2015 Christopher Snowhill
//
// Changes:
//  0.4.2  (2015-04-14): Removed usage of the popcnt opcode, which requires arbitrary new processors, and fixed profile string generation
//  0.4.1  (2015-04-13): Added 14 bit to 16 bit packing to packet decoder
//  0.4.0  (2015-04-13): Replaced libdca with libdcadec
//  0.3.3  (2014-02-16): Implemented support for packet decoding from MP4 files
//  0.3.2  (2013-11-03): Fixed channel mode reporting in the event that dts_flags has an invalid channel number field
//  0.3.1  (2013-01-31): Amended decode postprocessor to emit silence in the same format as the DTS signal
//  0.3.0  (2010-09-03): Added support for 48KHz source streams to the decode postprocessor
//  0.2.9  (2010-05-23): Implemented decode_postprocessor interface, removed DSP
//  0.2.8  (2010-01-11): Updated to 1.0 SDK
//  0.2.7  (2009-12-13): Found and fixed another false positive with the DSP
//  0.2.6  (2009-12-13): Really fixed DTS decoder DSP this time
//  0.2.5  (2009-12-05): Fixed heap corruption on bad DTS files, DSP doesn't output until two consecutive frames are found
//  0.2.4  (2009-05-02): Fixed a bug in DTS DSP and packet decoder for when dca_syncinfo fails
//  0.2.3  (2009-03-30): Fixed tag writing
//  0.2.2  (2008-10-26): Restricted silence generation to DTS WAV files
//  0.2.1  (2008-09-28): Seeking accuracy improvements, SDK's Decoder Validator was broken and didn't notice the bugs
//  0.2.0  (2008-09-13): Added preliminary DSP decoder for use with DTS audio CDs and WAVs without changing extension
//  0.1.9  (2008-09-02): Small change: output silence instead of nothing when decoding a block fails
//  0.1.8  (2008-09-02): Tagging configuration is back (in Advanced preferences), updated libdca to version 0.0.5, seeking accuracy improved
//  0.1.7  (2006-07-22): Added support for internal cuesheets
//  0.1.6  (2006-07-12): Playback from cuesheets always started from the beginning of the file, fixed
//  0.1.5  (2006-07-09): foobar2000 v0.9.x conversion
//  0.1.4  (2005-07-24): Accepts header with reversed word order
//  0.1.3  (2005-03-28): Smarter channel order fixing. Now uses 9 channel output only for files that output to center rear speaker.
//                       Added channel order hack also to packet decoder, added configuration
//  0.1.2  (2005-03-28): Fixed crashing with invalid files
//  0.1.1  (2005-03-26): Hack to fix output channel order for odd channel modes
//  0.1    (2005-03-20): Fixed incorrect handling for files that had no LFE channel, more accurate seeking
//  0.0.9  (2005-01-01): Fixed hanging at the end of DTS files
//  0.0.8  (2004-12-27): Improved DTS-in-wav detection
//  0.0.7  (2004-10-23): Added DTS header seeking to support more DTS-in-wav files
//  0.0.6  (2004-10-20): Removed most of the changes for 0.0.5 and replaced with hacks for now
//  0.0.5  (2004-10-17): Fixes to raw DTS handling
//  0.0.4  (2004-10-15): Simplified packet decoder, added codec reporting, fixed typo in version number
//  0.0.3  (2004-10-15): Added Matroska packet decoder support

#define FD_VERSION  "0.4.2"

//#define DTS_DEBUG // print status info to console

#include "../SDK/foobar2000.h"
#include "../helpers/helpers.h"
using namespace pfc;

#include <intrin.h>

extern "C" {
#include <libdcadec/common.h>
#include <libdcadec/dca_stream.h>
#include <libdcadec/dca_context.h>
}

enum {
	BUFFER_SIZE = 24576,
	HEADER_SIZE = 14,
	FRAME_SAMPLES = 256,


	cfg_drc = 0 // disable dynamic range compression
};

//cfg_int cfg_drc ( "drc", 0 );   // Dynamic range compression defaults to off
//cfg_int cfg_tag ( "tag", 1 );   // Write APEv2 tags by default

static const GUID guid_dts_branch = { 0x879389e3, 0x8c4, 0x4db0, { 0x8c, 0xb7, 0xec, 0x51, 0x3d, 0xb5, 0xd9, 0x14 } };
static const GUID guid_enable_tag = { 0x39e19ab2, 0xfb84, 0x4657, { 0xbf, 0x8c, 0x7a, 0x55, 0x7a, 0x35, 0xce, 0x72 } };
//static const GUID guid_enable_drc = { 0xe254b211, 0xc3f4, 0x40b6, { 0x91, 0xc, 0xa8, 0x3e, 0xe, 0x7f, 0x61, 0x2f } };
static advconfig_branch_factory dts_tagging_branch("DTS", guid_dts_branch, advconfig_branch::guid_branch_tagging, 0);
static advconfig_checkbox_factory g_cfg_tag("Enable APEv2 tag writing", guid_enable_tag, guid_dts_branch, 0, true);
//static advconfig_checkbox_factory g_cfg_drc("DTS - Use dynamic range compression", guid_enable_drc, advconfig_branch::guid_branch_decoding, 0, false);

// -------------------------------------

void info_set_dts_profile(file_info & p_info, int profile)
{
	pfc::string8 profile_string;
	if (profile)
	{
		for (int i = 1; i <= profile; i <<= 1)
		{
			if (profile & i)
			{
				if (profile_string.length()) profile_string << " ";
				switch (i)
				{
				case DCADEC_PROFILE_DS: profile_string << "Digital Surround"; break;
				case DCADEC_PROFILE_DS_96_24: profile_string << "Digital Surround 96/24"; break;
				case DCADEC_PROFILE_DS_ES: profile_string << "Digital Surround ES"; break;
				case DCADEC_PROFILE_HD_HRA: profile_string << "High-Resolution Audio"; break;
				case DCADEC_PROFILE_HD_MA: profile_string << "Master Audio"; break;
				case DCADEC_PROFILE_EXPRESS: profile_string << "Express"; break;
				}
			}
		}
	}
	if (profile_string.length())
		p_info.info_set("codec_profile", profile_string);
	p_info.info_set("encoding", profile == DCADEC_PROFILE_HD_MA ? "lossless" : "lossy");
}

struct dcadec_stream_handle
{
	file::ptr f;
	abort_callback * p_abort;
};

static int dcadec_seek(void * opaque, off_t offset, int whence)
{
	dcadec_stream_handle * h = (dcadec_stream_handle *)opaque;
	try
	{
		switch (whence)
		{
		case SEEK_CUR:
			offset += h->f->get_position(*h->p_abort);
			break;

		case SEEK_END:
			offset += h->f->get_size_ex(*h->p_abort);
			break;
		}
		
		h->f->seek(offset, *h->p_abort);

		return 0;
	}
	catch (...)
	{
		return -1;
	}
}

static off_t dcadec_tell(void * opaque)
{
	dcadec_stream_handle * h = (dcadec_stream_handle *)opaque;
	try
	{
		return h->f->get_position(*h->p_abort);
	}
	catch (...)
	{
		return -1;
	}
}

static int dcadec_getc(void * opaque)
{
	dcadec_stream_handle * h = (dcadec_stream_handle *)opaque;
	unsigned char byte;
	try
	{
		if (!h->f->read(&byte, 1, *h->p_abort))
		{
			return EOF;
		}
		else
		{
			return byte;
		}
	}
	catch (...)
	{
		return EOF;
	}
}

static size_t dcadec_read(void * opaque, void * buf, size_t count)
{
	dcadec_stream_handle * h = (dcadec_stream_handle *)opaque;
	try
	{
		return h->f->read(buf, count, *h->p_abort);
	}
	catch (...)
	{
		return -1;
	}
}

static const dcadec_stream_callbacks dcadec_cb =
{
	dcadec_seek,
	dcadec_tell,
	dcadec_getc,
	dcadec_read
};

static void parse_tagtype_internal(const char *p_tagtype, bool &p_have_id3v1, bool &p_have_id3v2, bool &p_have_apev2)
{
    const char *tagtype = p_tagtype;
    while(*tagtype)
    {
        unsigned delta = 0;
        while(tagtype[delta] != 0 && tagtype[delta] != '|') delta++;
        if (delta > 0)
        {
            if (!stricmp_utf8_ex(tagtype, delta, "apev1", ~0) || !stricmp_utf8_ex(tagtype, delta, "apev2", ~0))
                p_have_apev2 = true;
            else if (!stricmp_utf8_ex(tagtype, delta, "id3v1", ~0))
                p_have_id3v1 = true;
            else if (!stricmp_utf8_ex(tagtype, delta, "id3v2", ~0))
                p_have_id3v2 = true;
        }
        tagtype += delta;
        while(*tagtype == '|') tagtype++;
    }
}

class input_dts {
private:
    service_ptr_t<file> r, r_;
    long frame_size;
	struct dcadec_stream *stream;
	struct dcadec_context *state;
	struct dcadec_stream_handle handle;
    t_filesize header_pos;
    bool eof, iswav;

    /*
    uint8_t buf[BUFFER_SIZE];
    uint8_t *bufptr, *bufpos;
    */

    t_filesize tag_offset;
    int dts_flags, nch, srate, bps, profile, frame_length;
	uint32_t packed;
    double real_bitrate;
    unsigned int skip_samples;
	unsigned int skip_decoding_packets;
    unsigned int channel_mask;
    unsigned int silence_bytes;

	pfc::array_t<int32_t> interleaved_samples;

	__int64 skip_wav_header(file::ptr & r, abort_callback &p_abort)
	{
		t_filesize pos = r->get_position(p_abort);
		t_filesize filesize = r->get_size(p_abort);

		for (;;) {
			char temp[4];
			DWORD tmp;
			r->read(temp, 4, p_abort);
			if (memcmp(temp, "RIFF", 4)) break;
			r->read(temp, 4, p_abort);
			r->read(temp, 4, p_abort);
			if (memcmp(temp, "WAVE", 4)) break;

			for (;;) {
				if (r->read(temp, 4, p_abort) != 4) break;
				if (!memcmp(temp, "fmt ", 4)) break; //success
				if (!memcmp(temp, "data", 4)) break; //already got data chunk but no fmt
				if (r->read(&tmp, 4, p_abort) != 4) break;
				if (tmp & 1) tmp++;
				if (tmp == 0 || r->get_position(p_abort) + tmp > filesize - 8) break;
				r->seek(tmp + r->get_position(p_abort), p_abort);
			}

			if (memcmp(temp, "fmt ", 4)) break;

			__int64 position = r->get_position(p_abort) - 4;

			if (r->read(&tmp, 4, p_abort) != 4) break;
			if (tmp < 14 || tmp > 64 * 1024) break;
			r->seek(tmp + r->get_position(p_abort), p_abort);

			char code[4];

			do {
				position += 8 + tmp + (tmp & 1); //word-align all blocksizes
				r->seek(position, p_abort);
				if (r->read(code, 4, p_abort) != 4) break;
				if (r->read(temp, 4, p_abort) != 4) break;
			} while (memcmp(code, "data", 4));

			if (memcmp(code, "data", 4)) break;

			position += 8;
			r->seek(position, p_abort);
			return position - pos;
		}

		r->seek(pos, p_abort);
		return 0;
	}

public:
    input_dts()
    {
		stream = 0;
        state = 0;
    }

    ~input_dts()
    {
		if (state) dcadec_context_destroy(state);
		if (stream) dcadec_stream_close(stream);
    }

    bool decode_can_seek() { return r->can_seek(); }
    bool decode_get_dynamic_info(file_info &p_out, double &p_timestamp_delta) { return false; }
    bool decode_get_dynamic_info_track(file_info &p_out, double &p_timestamp_delta) { return false; }
    void decode_on_idle(abort_callback &p_abort) { r->on_idle(p_abort); }

    static bool g_is_our_content_type(const char *p_content_type)
    {
        return (!strcmp(p_content_type, "audio/x-dts")) || (!strcmp(p_content_type, "audio/dts"));
    }

    static bool g_is_our_path(const char *p_path, const char *p_extension)
    {
        return (stricmp_utf8(p_extension, "dts") == 0) || (stricmp_utf8(p_extension, "dtswav") == 0);
    }

    t_filestats get_file_stats(abort_callback &p_abort) { return r->get_stats(p_abort); }

    void open(service_ptr_t<file> p_filehint, const char *p_path, t_input_open_reason p_reason, abort_callback &p_abort)
    {
        if (p_reason==input_open_info_write && g_cfg_tag.get_static_instance().get_state()==false) throw exception_io_unsupported_format();

        r_ = p_filehint;
        input_open_file_helper(r_, p_path, p_reason, p_abort);

        tag_offset = 0;

        try {
            file_info_impl t;
            tag_processor::read_trailing_ex(r_, t, tag_offset, p_abort);
        } catch(exception_io_data) { /*file had no trailing tags */ }

        if (tag_offset == 0) tag_offset = r_->get_size(p_abort);

        header_pos = 0;
        tag_processor::skip_id3v2(r_, header_pos, p_abort);
        __int64 t = skip_wav_header(r_, p_abort);
        if (t > 0) iswav = true; else iswav = false;
        header_pos += t;

		//r = reader_limited::g_create(r_, 0, tag_offset, p_abort);
		service_ptr_t<reader_limited> r = new service_impl_t<reader_limited>();
		r->init(r_, header_pos, tag_offset, p_abort);
		this->r = r;
		
		handle.f = r;
		handle.p_abort = &p_abort;

		stream = dcadec_stream_open(&dcadec_cb, &handle);
		if (!stream) {
			throw exception_io_data("Failed to initialize DTS decoder");
		}

		nch = srate = dts_flags = frame_size = 0;
        skip_samples = 0;
        silence_bytes = 0;
#ifdef DTS_DEBUG
        skipped_bytes = 0;
        decoded_bytes = 0;
#endif
        eof = false;
        channel_mask = 0;

		state = dcadec_context_create(DCADEC_FLAG_STRICT);
		if (!state)
		{
			throw std::bad_alloc();
		}

		uint8_t * packet;
		size_t packet_size;
		if (dcadec_stream_read(stream, &packet, &packet_size, &packed) < 0)
		{
			throw exception_io_data("Invalid DTS file");
		}

		int ret;

		if ((ret = dcadec_context_parse(state, packet, packet_size)) < 0) {
			pfc::string8 exception;
			exception << "Error parsing packet: " << dcadec_strerror(ret);
			throw exception_io_data(exception);
		}

		int **samples, nsamples, channel_mask, sample_rate, bits_per_sample, profile;

		if ((ret = dcadec_context_filter(state, &samples, &nsamples,
			&channel_mask, &sample_rate,
			&bits_per_sample, &profile)) < 0) {
			pfc::string8 exception;
			exception << "Error filtering frame: " << dcadec_strerror(ret);
			throw exception_io_data(exception);
		}

		nch = dca_popcount(channel_mask);
		srate = sample_rate;
		bps = bits_per_sample;
        if (!srate || !packet_size || !nch) {
            dcadec_context_destroy(state);
            state = 0;
			dcadec_stream_close(stream);
			stream = 0;
            throw exception_io_data();
        }

        this->channel_mask = channel_mask;
		frame_length = nsamples;
		frame_size = packet_size;
		if (packed)
			frame_size += frame_size / 7;
		this->profile = profile;
    }

    void get_info(file_info &p_info, abort_callback &p_abort)
    {
        t_filesize current_pos = r->get_position(p_abort);
		t_filesize next_pos;
		__int64 offset;
        //t_filesize tag_offset = r->get_size(p_abort);

        try {
            //tag_processor::read_id3v2(r, p_info, p_abort);
            tag_processor::read_id3v2_trailing(r_, p_info, p_abort);
        } catch(exception_io_data) { /*file had no tags */ }

		r->seek(current_pos, p_abort);

		handle.p_abort = &p_abort;

		dcadec_stream_reset(stream);
		next_pos = r->get_position(p_abort);
		offset = next_pos - current_pos; // Presumably will be set to -SYNC_SIZE if a sync packet is buffered
		r->seek(0, p_abort);

		size_t packet_count = 0;
		size_t total_packet_size = 0;
		for (;;)
		{
			int ret;
			uint8_t * packet;
			size_t packet_size;
			if ((ret = dcadec_stream_read(stream, &packet, &packet_size, &packed)) < 0)
			{
				throw exception_io_data("Invalid DTS file");
			}
			if (ret)
			{
				++packet_count;
				total_packet_size += packet_size;
			}
			else break;
		}

        //try {
        //    tag_processor::read_trailing_ex(r, p_info, tag_offset, p_abort);
        //} catch(exception_io_data) { /*file had no trailing tags */ }
        
		dcadec_stream_reset(stream);
		r->seek(current_pos + offset, p_abort);

        double length = (double)frame_length * (double)packet_count / (double)srate;
		real_bitrate = (((double)total_packet_size / (double)packet_count) * 8.0) * (double)srate / (double)frame_length;

        p_info.set_length(length);
        p_info.info_set_int("samplerate", srate);
        p_info.info_set_int("channels", nch);
        p_info.info_set_int("bitrate", (int)((real_bitrate+500)/1000));
		p_info.info_set_int("bitspersample", bps);
        p_info.info_set("codec", "DTS");
		info_set_dts_profile(p_info, profile);
    }

    void decode_initialize(unsigned p_flags, abort_callback &p_abort)
    {
        r->reopen(p_abort); //equivalent to seek to zero, except it also works on nonseekable streams
        r->seek(0, p_abort);
		if (stream)
			dcadec_stream_close(stream);
		handle.p_abort = &p_abort;
		stream = dcadec_stream_open(&dcadec_cb, &handle);
		if (!stream)
		{
			throw exception_io_data("Unable to open DTS file");
		}
		dcadec_context_clear(state);
		skip_decoding_packets = 0;
		skip_samples = 0;
		eof = false;
    }

    bool decode_run(audio_chunk &chunk, abort_callback &p_abort)
    {
        if (eof) return false;

		handle.p_abort = &p_abort;

		for (;;)
		{
			int ret;
			uint8_t * packet;
			size_t packet_size;

			for (;;)
			{
				if ((ret = dcadec_stream_read(stream, &packet, &packet_size, NULL)) < 0)
				{
					throw exception_io_data("Error reading DTS packet");
				}
				if (!ret)
					break;
				if (skip_decoding_packets)
					--skip_decoding_packets;
				else
					break;
			}

			if (ret == 0)
			{
				eof = true;
				return false;
			}

			if ((ret = dcadec_context_parse(state, packet, packet_size)) < 0)
			{
				pfc::string8 exception;
				exception << "Error parsing DTS packet: " << dcadec_strerror(ret);
				throw exception_io_data(exception);
			}

			int **sample_data, nsamples, channel_mask, sample_rate, bits_per_sample, profile;

			if ((ret = dcadec_context_filter(state, &sample_data, &nsamples,
				&channel_mask, &sample_rate,
				&bits_per_sample, &profile)) < 0) {
				pfc::string8 exception;
				exception << "Error filtering frame: " << dcadec_strerror(ret);
				throw exception_io_data(exception);
			}

			if (skip_samples >= nsamples)
			{
				skip_samples -= nsamples;
				continue;
			}
			else
			{
				nsamples -= skip_samples;
			}

			int nch = dca_popcount(channel_mask);

			interleaved_samples.grow_size(nch * nsamples);

			for (int i = 0; i < nsamples; ++i)
			{
				for (int j = 0; j < nch; ++j)
				{
					int * input_sample = sample_data[j] + i + skip_samples;
					interleaved_samples[i * nch + j] = *input_sample;
				}
			}

			chunk.grow_data_size(nsamples * nch);
			audio_math::convert_from_int32(interleaved_samples.get_ptr(), nsamples * nch, chunk.get_data(), 1ULL << (32 - bits_per_sample));

			skip_samples = 0;

			chunk.set_sample_count(nsamples);
			chunk.set_channels(nch, channel_mask);
			chunk.set_srate(sample_rate);

			return true;
		}
	}

    void decode_seek(double newpos, abort_callback &p_abort)
    {
        r->ensure_seekable(); //throw exceptions if someone called decode_seek() despite of our input having reported itself as nonseekable.

        __int64 sample_pos = (__int64)(newpos*srate+0.5);
        t_filesize filesize = r->get_size(p_abort);

		handle.p_abort = &p_abort;

        if (sample_pos >= 0 && frame_size > 0) {
            eof = false;
            skip_samples = 0;
			skip_decoding_packets = 0;

            dcadec_context_clear(state);
			dcadec_stream_reset(stream);
			r->seek(0, p_abort);

			skip_decoding_packets = sample_pos / frame_length;
			if (skip_decoding_packets >= 20)
			{
				skip_decoding_packets -= 20;
				skip_samples = sample_pos - skip_decoding_packets * frame_length;
			}
			else
			{
				skip_decoding_packets = 0;
				skip_samples = sample_pos;
			}
        }
    }

    void retag(const file_info &p_info, abort_callback &p_abort)
    {
        if (g_cfg_tag.get_static_instance().get_state() == false) throw exception_io_unsupported_format(); // exit out when tags are not wanted
        r->ensure_seekable();

        const char *tagtype = p_info.info_get("tagtype");
        if (tagtype == 0) tagtype = "";

        bool id3v1=false, id3v2=false, apev2=false;
        parse_tagtype_internal(tagtype, id3v1, id3v2, apev2);

        if (!id3v2 && !apev2) apev2 = true;

        tag_processor::write_multi(r_, p_info, p_abort, id3v1, id3v2, apev2);

		tag_offset = 0;

		try {
			file_info_impl t;
			tag_processor::read_trailing_ex(r_, t, tag_offset, p_abort);
		}
		catch (exception_io_data) { /*file had no trailing tags */ }

		if (tag_offset == 0) tag_offset = r_->get_size(p_abort);

		header_pos = 0;
		tag_processor::skip_id3v2(r_, header_pos, p_abort);
		__int64 t = skip_wav_header(r_, p_abort);
		if (t > 0) iswav = true; else iswav = false;
		header_pos += t;

		this->r.release();
		service_ptr_t<reader_limited> r = new service_impl_t<reader_limited>();
		r->init(r_, header_pos, tag_offset, p_abort);
		this->r = r;
    }

    bool decode_run_raw(audio_chunk &p_chunk, mem_block_container &p_raw, abort_callback &p_abort) { throw pfc::exception_not_implemented(); }
    void set_logger(event_logger::ptr ptr) {}
};

static input_cuesheet_factory_t<input_dts> g_input_dts_factory;

DECLARE_COMPONENT_VERSION("DTS decoder", FD_VERSION,
  "DTS decoding powered by libdcadec by foo86.\n"
  "Homepage for libdcadec: http://github.com/foo86/dcadec\n"
  "Homepage for my fork:   http://github.com/kode54/dcadec\n"
  "\n"
  "foobar2000 component by Janne Hyvärinen and Chris Moeller.\n"
  "Licensed under GNU GPL.\n");

DECLARE_FILE_TYPE("DTS files", "*.DTS;*.DTSWAV");

VALIDATE_COMPONENT_FILENAME("foo_input_dts.dll");
