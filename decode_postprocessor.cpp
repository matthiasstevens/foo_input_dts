#include "../SDK/foobar2000.h"

#include <stdint.h>

#include <intrin.h>

extern "C" {
#include <libdcadec/common.h>
#include <libdcadec/bitstream.h>
#include <libdcadec/dca_context.h>
#include <libdcadec/dca_stream.h>
}

enum {
	BUFFER_SIZE = 24576,
	HEADER_SIZE = 16,
	SYNC_SIZE = 4,
	FRAME_SAMPLES = 256
};

extern void info_set_dts_profile(file_info & p_info, int profile);

bool is_chunk_silent( audio_chunk * chunk )
{
	audio_sample * data = chunk->get_data();
	for ( unsigned i = 0, j = chunk->get_data_length(); i < j; i++ )
	{
		if ( data[ i ] ) return false;
	}
	return true;
}

static void swap16(uint32_t *data, size_t size)
{
	while (size--) {
		uint32_t v = *data;
		*data++ = ((v & 0x00ff00ff) << 8) | ((v & 0xff00ff00) >> 8);
	}
}

class dts_postprocessor_instance : public decode_postprocessor_instance
{
	dsp_chunk_list_impl original_chunks;
	dsp_chunk_list_impl output_chunks;

	struct dcadec_context *state;

	pfc::array_t<audio_sample> output_samples;
	pfc::array_t<uint8_t> buffer;
	uint8_t buf[BUFFER_SIZE];
	uint8_t *bufptr, *bufpos;
	bool valid_stream_found;

	pfc::array_t<int32_t> interleaved_samples;

	int nch, sample_rate, channel_mask, bits_per_sample, profile, frame_length, frame_size;

	bool swap;

	unsigned int contiguous_silent_samples;
	unsigned int contiguous_silent_bytes;
	unsigned int pre_silent_samples;
	unsigned int post_silent_samples;
	unsigned int bytes_skipped;

	bool info_emitted, gave_up;

	bool init()
	{
		cleanup();

		state = dcadec_context_create(DCADEC_FLAG_STRICT);
		if (!state)
		{
			console::error("Failed to initialize DTS decoder");
			return false;
		}

		return true;
	}

    void cleanup()
    {
		if (state)
		{
			dcadec_context_destroy(state);
			state = 0;
		}

		original_chunks.remove_all();
		output_chunks.remove_all();

		bufptr = buf;
		bufpos = buf + HEADER_SIZE;
		nch = sample_rate = bits_per_sample = channel_mask = profile = frame_length = frame_size = 0;
		contiguous_silent_samples = 0;
		contiguous_silent_bytes = 0;
		pre_silent_samples = 0;
		post_silent_samples = 0;
		bytes_skipped = 0;
		valid_stream_found = false;
		info_emitted = false;
		gave_up = false;
	}

	bool decode( const void *data, t_size bytes, audio_chunk & p_chunk, abort_callback & p_abort )
    {
		uint8_t *start = (uint8_t *)data;
		uint8_t *end = (uint8_t *)data + bytes;
		unsigned int samples = 0;

		uint32_t sync, saved_sync;
		bool packed = false;
		size_t packed_size;

		while (1)
		{
			unsigned len = end - start;
			if ( !len ) break;
			if ( len > bufpos - bufptr ) len = bufpos - bufptr;

			memcpy( bufptr, start, len );
			bufptr += len;
			start += len;

			if ( bufptr == bufpos )
			{
				if ( bufpos == buf + HEADER_SIZE )
				{
					frame_size = 0;
					packed = false;

					memcpy(&sync, buf, 4);
					sync = DCA_32BE(sync);
					if (sync == 0x1FFFE800
						|| sync == 0xFF1F00E8)
					{
						packed_size = dcadec_stream_pack(buf, buf, HEADER_SIZE / 8, sync);
						saved_sync = sync;
						memcpy(&sync, buf, 4);
						sync = DCA_32BE(sync);
						packed = true;
					}
					if (sync == SYNC_WORD_CORE
						|| sync == SYNC_WORD_EXSS
						|| sync == SYNC_WORD_CORE_LE
						|| sync == SYNC_WORD_EXSS_LE)
					{
						swap = false;

						switch (sync)
						{
						case SYNC_WORD_CORE_LE:
							sync = SYNC_WORD_CORE;
							swap = true;
							break;

						case SYNC_WORD_EXSS_LE:
							sync = SYNC_WORD_EXSS;
							swap = true;
							break;
						}

						if (swap)
							swap16((uint32_t *)buf, HEADER_SIZE / 4);

						struct bitstream bits;

						bits_init(&bits, buf, HEADER_SIZE);
						bits_skip(&bits, 32);

						if (sync == SYNC_WORD_CORE) {
							bool normal_frame = bits_get1(&bits);
							int deficit_samples = bits_get(&bits, 5) + 1;
							if (normal_frame && deficit_samples != 32)
								goto error;
							bits_skip1(&bits);
							int npcmblocks = bits_get(&bits, 7) + 1;
							if (npcmblocks < 6)
								goto error;
							frame_size = bits_get(&bits, 14) + 1;
							if (frame_size < 96)
								goto error;
							if (packed)
								frame_size += frame_size / 7;
						}
						else {
							bits_skip(&bits, 10);
							bool wide_hdr = bits_get1(&bits);
							bits_skip(&bits, 8 + 4 * wide_hdr);
							frame_size = bits_get(&bits, 16 + 4 * wide_hdr) + 1;
							if (frame_size < HEADER_SIZE)
								goto error;
						}
					}

					if ( !frame_size )
					{
						//console::warning("DTS: skip");
						++bytes_skipped;
						if ( !bufptr[0] )
						{
							if ( ( ++contiguous_silent_bytes & 3 ) == 0 &&
								( bytes_skipped & 3 ) == 0 ) ++contiguous_silent_samples;
						}
						else
						{
							contiguous_silent_samples = 0;
							contiguous_silent_bytes = 0;
						}
						for ( bufptr = buf; bufptr < buf + HEADER_SIZE - 1; bufptr++ ) bufptr[0] = bufptr[1];
						continue;
					}
					else
						bytes_skipped += frame_size;

					bufpos = buf + frame_size;
				}
				else
				{
					int ret;

					if (packed)
					{
						packed_size += dcadec_stream_pack(buf + packed_size, buf + HEADER_SIZE, (frame_size - HEADER_SIZE + 7) / 8, saved_sync);
						frame_size = packed_size;
					}
					else
					{
						if (swap)
							swap16((uint32_t *)(buf + HEADER_SIZE), (frame_size - HEADER_SIZE) / 4);
					}

					if ((ret = dcadec_context_parse(state, buf, frame_size)) < 0)
					{
						pfc::string8 exception;
						exception << "Error parsing DTS packet: " << dcadec_strerror(ret);
						console::print(exception);
						goto error;
					}

					int **sample_data, nsamples;

					if ((ret = dcadec_context_filter(state, &sample_data, &nsamples,
						&channel_mask, &sample_rate,
						&bits_per_sample, &profile)) < 0) {
						pfc::string8 exception;
						exception << "Error filtering frame: " << dcadec_strerror(ret);
						console::print(exception);
						goto error;
					}
					
					nch = dca_popcount(channel_mask);

					interleaved_samples.grow_size(nch * nsamples);

					for (int i = 0; i < nsamples; ++i)
					{
						for (int j = 0; j < nch; ++j)
						{
							int * input_sample = sample_data[j] + i;
							interleaved_samples[i * nch + j] = *input_sample;
						}
					}

					output_samples.grow_size((samples + nsamples) * nch);
					audio_math::convert_from_int32(interleaved_samples.get_ptr(), nsamples * nch, output_samples.get_ptr() + samples * nch, 1ULL << (32 - bits_per_sample));
					samples += nsamples;

					bufptr = buf;
					bufpos = buf + HEADER_SIZE;
					continue;

error:
					return false;
					//console::warning("DTS: error");
					bufptr = buf;
					bufpos = buf + HEADER_SIZE;
				}
			}
		}

		if (samples >= FRAME_SAMPLES)
		{
			p_chunk.set_data( output_samples.get_ptr(), samples, nch, sample_rate, channel_mask );
			return true;
		}
		return false;
	}

	unsigned flush_chunks( dsp_chunk_list & p_chunk_list, unsigned insert_point, bool output = false )
	{
		dsp_chunk_list * list = output ? &output_chunks : &original_chunks;
		unsigned ret = list->get_count();
		if ( ret )
		{
			for ( unsigned i = 0; i < list->get_count(); i++ )
			{
				audio_chunk * in = list->get_item( i );
				audio_chunk * out = p_chunk_list.insert_item( insert_point++, in->get_data_length() );
				out->copy( *in );
			}
		}
		original_chunks.remove_all();
		output_chunks.remove_all();
		return ret;
	}

	unsigned flush_silence( dsp_chunk_list & p_chunk_list, unsigned insert_point, unsigned sample_count )
	{
		if ( sample_count )
		{
			audio_chunk * out = p_chunk_list.insert_item( insert_point++, sample_count * nch );
			out->set_srate( sample_rate );
			out->set_channels( nch, channel_mask );
			out->set_silence( sample_count );
			return 1;
		}
		else return 0;
	}

public:
	dts_postprocessor_instance()
	{
		state = 0;
		cleanup();
	}

	~dts_postprocessor_instance()
	{
		cleanup();
	}

	virtual bool run( dsp_chunk_list & p_chunk_list, t_uint32 p_flags, abort_callback & p_abort )
	{
		if ( gave_up || p_flags & flag_altered ) return false;

		bool modified = false;

		for ( unsigned i = 0; i < p_chunk_list.get_count(); )
		{
			audio_chunk * chunk = p_chunk_list.get_item( i );

			if ( chunk->get_channels() != 2 || ( chunk->get_srate() != 44100 && chunk->get_srate() != 48000 ) ) {
				i += flush_chunks( p_chunk_list, i, valid_stream_found ) + 1;
				continue;
			}

			if (!state)
			{
				if (!init()) break;
			}

			int data = chunk->get_sample_count() * 4;
			buffer.grow_size( data );
			audio_math::convert_to_int16( chunk->get_data(), chunk->get_sample_count() * 2, (t_int16 *)buffer.get_ptr(), 1.0 );

			if ( !valid_stream_found )
			{
				audio_chunk * out = original_chunks.insert_item( original_chunks.get_count(), chunk->get_data_length() );
				out->copy( *chunk );
				if ( decode( buffer.get_ptr(), data, *chunk, p_abort ) )
				{
					if ( output_chunks.get_count() )
					{
						valid_stream_found = true;
						i += flush_silence( p_chunk_list, i, pre_silent_samples );
						i += flush_chunks( p_chunk_list, i, true ) + 1;
						modified = true;
					}
					else
					{
						out = output_chunks.insert_item( output_chunks.get_count(), chunk->get_data_length() );
						out->copy( *chunk );
						p_chunk_list.remove_by_idx( i );
						pre_silent_samples = contiguous_silent_samples;
					}
				}
				else
				{
					p_chunk_list.remove_by_idx( i );
					output_chunks.remove_all();
				}
			}
			else
			{
				contiguous_silent_samples = 0;
				if ( decode( buffer.get_ptr(), data, *chunk, p_abort ) )
				{
					i++;
					modified = true;
				}
				else
				{
					p_chunk_list.remove_by_idx( i );
				}
				post_silent_samples += contiguous_silent_samples;
			}
		}

		if ( valid_stream_found )
		for ( unsigned i = 0; i < original_chunks.get_count(); )
		{
			audio_chunk * in = original_chunks.get_item( i );
			if ( !is_chunk_silent( in ) ) break;
			flush_silence( p_chunk_list, p_chunk_list.get_count(), in->get_sample_count() );
			original_chunks.remove_by_idx( i );
		}

		if ( original_chunks.get_duration() >= 1.0 )
		{
			flush_chunks( p_chunk_list, p_chunk_list.get_count() );
			gave_up = true;
		}

		if ( p_flags & flag_eof )
		{
			flush_chunks( p_chunk_list, p_chunk_list.get_count(), valid_stream_found );
			flush_silence( p_chunk_list, p_chunk_list.get_count(), post_silent_samples );
			cleanup();
		}

		return modified;
	}

	virtual bool get_dynamic_info( file_info & p_out )
	{
		if ( !info_emitted )
		{
			if ( valid_stream_found )
			{
				info_emitted = true;
				p_out.info_set_int( "samplerate", sample_rate );
				p_out.info_set_int( "channels", nch );
				//p_out.info_set_int( "bitrate", bitrate / 1000 );
				p_out.info_set( "codec", "DTS" );
				info_set_dts_profile(p_out, profile);
				return true;
			}
		}
		return false;
	}

	virtual void flush()
	{
		cleanup();
	}

	virtual double get_buffer_ahead()
	{
		return 16384. / 4. / 44100.;
	}
};

class dts_postprocessor_entry : public decode_postprocessor_entry
{
public:
	virtual bool instantiate( const file_info & info, decode_postprocessor_instance::ptr & out )
	{
        if ( info.info_get_decoded_bps() != 16 )
		{
            return false;
        }

		const char * encoding = info.info_get( "encoding" );
		if ( !encoding || pfc::stricmp_ascii( encoding, "lossless" ) )
		{
			return false;
		}

		out = new service_impl_t< dts_postprocessor_instance >;

		return true;
	}
};

static service_factory_single_t< dts_postprocessor_entry > g_dts_postprocessor_entry_factory;
