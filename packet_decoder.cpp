#include "../SDK/foobar2000.h"

#include <intrin.h>

extern "C" {
#include <libdcadec/common.h>
#include <libdcadec/dca_context.h>
#include <libdcadec/dca_stream.h>
}

#define BUFFER_SIZE 24576
#define HEADER_SIZE 14
#define FRAME_SAMPLES 256
static const int mp4_dts = 169;

extern void info_set_dts_profile(file_info & p_info, int profile);

class packet_decoder_dts : public packet_decoder {
    bool init()
    {
        cleanup();

        state = dcadec_context_create(DCADEC_FLAG_STRICT);
        if (!state) {
            console::error("Failed to initialize DTS decoder");
            return false;
        }

        return true;
    }

    void cleanup()
    {
        if (state) {
            dcadec_context_destroy(state);
            state = NULL;
        }

        channel_mask = nch = srate = profile = 0;
    }

    struct dcadec_context *state;

    int channel_mask, nch, srate, frame_length, profile;

	pfc::array_t<uint8_t> packet_copy;

	pfc::array_t<int32_t> interleaved_samples;

public:
    packet_decoder_dts()
    {
        state = 0;

        cleanup();
    }

    ~packet_decoder_dts()
    {
        cleanup();
    }

    virtual t_size set_stream_property ( const GUID &p_type, t_size p_param1, const void *p_param2, t_size p_param2size )
    {
        return 0;
    }

    virtual void get_info ( file_info &p_info )
    {
        if (srate != 0) {
            p_info.info_set_int("channels", nch);
            p_info.info_set_int("samplerate", srate);
        }

        p_info.info_set("codec", "DTS");
		info_set_dts_profile(p_info, profile);
	}

    void open(const GUID &p_owner, bool p_decode, t_size p_param1, const void *p_param2, t_size p_param2size, abort_callback &p_abort)
    {
        if (!init()) throw exception_io_data();
        if (p_owner == owner_matroska) {
            const matroska_setup *setup = (const matroska_setup *)p_param2;
            nch = setup->channels;
            srate = setup->sample_rate;
        }
    }
    static bool g_is_our_setup(const GUID &p_owner, t_size p_param1, const void *p_param2, t_size p_param2size)
    {
        if (p_owner == owner_matroska) {
            if (p_param2size == sizeof(matroska_setup)) {
                const matroska_setup *setup = (const matroska_setup *)p_param2;
                if (!strncmp(setup->codec_id,"A_DTS", 5)) return true; else return false;
            } else return false;
        } else if (p_owner == owner_MP4) {
            if (p_param1 == mp4_dts) {
                return true;
            } else return false;
        }
        else return false;
    }

    virtual unsigned get_max_frame_dependency() { return 0; }
    virtual double get_max_frame_dependency_time() { return 0; }

    virtual void reset_after_seek() {}

    virtual bool analyze_first_frame_supported() { return (nch == 0); }

    virtual void analyze_first_frame(const void *p_buffer, t_size p_bytes, abort_callback &p_abort) 
    { 
        audio_chunk_impl dummy;
        decode(p_buffer, p_bytes, dummy, p_abort);
    }

    virtual void decode ( const void *data, t_size bytes, audio_chunk &p_chunk, abort_callback &p_abort )
    {
		t_size aligned_bytes = (bytes + 7) & ~7;
		size_t packed_size;

		packet_copy.grow_size(aligned_bytes);
		memcpy(packet_copy.get_ptr(), data, bytes);
		memset(packet_copy.get_ptr() + bytes, 0, aligned_bytes - bytes);

		packed_size = dcadec_stream_pack(packet_copy.get_ptr(), packet_copy.get_ptr(), aligned_bytes / 8, 0);
		if (packed_size < aligned_bytes)
		{
			bytes = packed_size;
			memset(packet_copy.get_ptr() + bytes, 0, aligned_bytes - bytes);
		}

		int ret;
		if ((ret = dcadec_context_parse(state, packet_copy.get_ptr(), bytes)) < 0)
		{
			pfc::string8 exception;
			exception << "Error parsing DTS stream: " << dcadec_strerror(ret);
			throw exception_io_data(exception);
		}

		int **sample_data, nsamples, channel_mask, sample_rate, bits_per_sample, profile;

		if ((ret = dcadec_context_filter(state, &sample_data, &nsamples,
			&channel_mask, &sample_rate,
			&bits_per_sample, &profile)) < 0) {
			pfc::string8 exception;
			exception << "Error filtering frame: " << dcadec_strerror(ret);
			throw exception_io_data(exception);
		}

		nch = dca_popcount(channel_mask);

		interleaved_samples.grow_size(nch * nsamples);

		for (int i = 0; i < nsamples; ++i)
		{
			for (int j = 0; j < nch; ++j)
			{
				int * input_sample = sample_data[j] + i;
				interleaved_samples[i * nch + j] = *input_sample;
			}
		}

		p_chunk.grow_data_size(nsamples * nch);
		audio_math::convert_from_int32(interleaved_samples.get_ptr(), nsamples * nch, p_chunk.get_data(), 1ULL << (32 - bits_per_sample));

		p_chunk.set_sample_count(nsamples);
		p_chunk.set_channels(nch, channel_mask);
		p_chunk.set_srate(sample_rate);
	}
};

static packet_decoder_factory_t<packet_decoder_dts> g_packet_decoder_dts_factory;
